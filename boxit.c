/*
 * This file is part of tss.
 * Copyright (C) 2022 Abdullah Çırağ
 *
 * tss is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU Affero General Public License as published by  
 * the Free Software Foundation, version 3 of the License only.  
 *
 * This program is distributed in the hope that it will be useful,  
 * but WITHOUT ANY WARRANTY; without even the implied warranty of  
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 * GNU Affero General Public License for more details.  
 *
 * You should have received a copy of the GNU Affero General Public License  
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.  
 */
#include "boxit.h"
#include <stdlib.h>
Box *boxes[MAX_BOX_COUNT];
size_t box_count = 0;

int
boxit_close()
{
	size_t i;
	for (i = 0; i < box_count; i++) {
		switch (boxes[i]->btype) {
		case BOX_BOXBCFB:
			free(boxes[i]->background_colors);
		case BOX_BOXBCF:
			free(boxes[i]->foreground_colors);
		case BOX_BOXBS:
		case BOX_BOXBC:
		case BOX_BOXB:
			free(boxes[i]->chars);
			break;
		}
		free(boxes[i]);
	}
}
