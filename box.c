/*
 * This file is part of tss.
 * Copyright (C) 2022 Abdullah Çırağ
 *
 * tss is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU Affero General Public License as published by  
 * the Free Software Foundation, version 3 of the License only.  
 *
 * This program is distributed in the hope that it will be useful,  
 * but WITHOUT ANY WARRANTY; without even the implied warranty of  
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 * GNU Affero General Public License for more details.  
 *
 * You should have received a copy of the GNU Affero General Public License  
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.  
 */
#include "box.h"
#include <stdlib.h>
#include <string.h>
#include "boxit.h"

Box *
box_create(unsigned int boxwidth, unsigned int boxheight)
{
	if (box_count == MAX_BOX_COUNT)
		return NULL;
	Box *b;
	b = (Box *) malloc(sizeof(Box));
	b->min_width = boxwidth;
	b->min_height = boxheight;
	b->btype = BOX_BOXB;
	b->chars = (char *) malloc(boxwidth * boxheight + 1);
	memset(b->chars, EMPTY_CHAR, boxwidth * boxheight);
	b->char_count = 0;
	b->foreground_colors = NULL;
	b->background_colors = NULL;
	b->foreground_color_count = 0;
	b->background_color_count = 0;
	boxes[box_count++] = b;
	return b;
}

Box *
box_create_c(unsigned int boxwidth, unsigned int boxheight, char chr)
{
	if (box_count == MAX_BOX_COUNT)
		return NULL;
	Box *b;
	b = (Box *) malloc(sizeof(Box));
	b->min_width = boxwidth;
	b->min_height = boxheight;
	b->btype = BOX_BOXBC;
	b->chars = (char *) malloc(boxwidth * boxheight + 1);
	memset(b->chars, chr, boxwidth * boxheight);
	b->char_count = 0;
	b->foreground_colors = NULL;
	b->background_colors = NULL;
	/*unsigned int fcolor[3] = {255, 255, 255};
	b->foreground_colors = (unsigned int (*)[3]) malloc(sizeof(fgcolor));
	memcpy(*b->foreground_colors, fgcolor, sizeof(fgcolor));*/
	b->foreground_color_count = 0;
	b->background_color_count = 0;
	boxes[box_count++] = b;
	return b;
}

Box *
box_create_cf(unsigned int boxwidth, unsigned int boxheight, char chr,
        unsigned int (*fgcolor)[3], unsigned int fgccount)
{
	if (box_count == MAX_BOX_COUNT)
		return NULL;
	Box *b;
	b = (Box *) malloc(sizeof(Box));
	b->min_width = boxwidth;
	b->min_height = boxheight;
	b->btype = BOX_BOXBCF;
	b->chars = (char *) malloc(boxwidth * boxheight + 1);
	memset(b->chars, chr, boxwidth * boxheight);
	b->char_count = 0;
	b->foreground_colors = (unsigned int (*)[3])
	        malloc(sizeof(*fgcolor) * fgccount);
	b->background_colors = NULL;
	memcpy(*b->foreground_colors, *fgcolor, sizeof(*fgcolor) * fgccount);
	b->foreground_color_count = fgccount;
	b->background_color_count = 0;
	boxes[box_count++] = b;
	return b;
}

Box *
box_create_cfb(unsigned int boxwidth, unsigned int boxheight, char chr,
        unsigned int (*fgcolor)[3], unsigned int fgccount,
        unsigned int (*bgcolor)[3], unsigned int bgccount)
{
	if (box_count == MAX_BOX_COUNT)
		return NULL;
	Box *b;
	b = (Box *) malloc(sizeof(Box));
	b->min_width = boxwidth;
	b->min_height = boxheight;
	b->btype = BOX_BOXBCFB;
	b->chars = (char *) malloc(boxwidth * boxheight + 1);
	memset(b->chars, chr, boxwidth * boxheight);
	b->char_count = 0;
	b->foreground_colors = (unsigned int (*)[3])
	        malloc(sizeof(*fgcolor) * fgccount);
	memcpy(*b->foreground_colors, *fgcolor, sizeof(*fgcolor) * fgccount);
	b->foreground_color_count = fgccount;
	b->background_colors = (unsigned int (*)[3])
	        malloc(sizeof(*bgcolor) * bgccount);
	memcpy(*b->background_colors, *bgcolor, sizeof(*bgcolor) * bgccount);
	b->background_color_count = bgccount;
	boxes[box_count++] = b;
	return b;
}

Box	*
box_create_s(unsigned int boxwidth, unsigned int boxheight,
        char *str, unsigned int chrcount)
{
	if (box_count == MAX_BOX_COUNT)
		return NULL;
	Box *b;
	b = (Box *) malloc(sizeof(Box));
	b->min_width = boxwidth;
	b->min_height = boxheight;
	b->btype = BOX_BOXBS;
	b->chars = (char *) malloc(boxwidth * boxheight + 1);
	strncpy(b->chars, str, chrcount);
	memset(b->chars + chrcount, EMPTY_CHAR, boxwidth * boxheight - chrcount + 1);
	b->char_count = chrcount;
    b->foreground_colors = NULL;
    b->background_colors = NULL;
	b->foreground_color_count = 0;
	b->background_color_count = 0;
	boxes[box_count++] = b;
	return b;
}
