/*
 * This file is part of tss.
 * Copyright (C) 2022 Abdullah Çırağ
 *
 * tss is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU Affero General Public License as published by  
 * the Free Software Foundation, version 3 of the License only.  
 *
 * This program is distributed in the hope that it will be useful,  
 * but WITHOUT ANY WARRANTY; without even the implied warranty of  
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 * GNU Affero General Public License for more details.  
 *
 * You should have received a copy of the GNU Affero General Public License  
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.  
 */
#include "boxit.h"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define BOX_HEIGHT 9
#define BOX_WIDTH 31

int main(void)
{
	unsigned int frame = 0;
	unsigned int random_fgcolors[BOX_WIDTH * BOX_HEIGHT][3];
	unsigned int random_bgcolors[BOX_WIDTH * BOX_HEIGHT][3];
	srand(time(0));
	for (int i = 0; i < BOX_WIDTH * BOX_HEIGHT; i++) {
		for (int j = 0; j < 3; j++) {
			random_fgcolors[i][j] = rand() % 256;
			random_bgcolors[i][j] = rand() % 256;
		}
	}
	/* Box* a = box_create_cfb(BOX_WIDTH, BOX_HEIGHT, '@',
	        random_fgcolors, BOX_WIDTH * BOX_HEIGHT,
	        random_bgcolors, BOX_WIDTH * BOX_HEIGHT); */
	Box* a = box_create_c(BOX_WIDTH, BOX_HEIGHT, '@');
	while (1 == 1) {
		printf("\x1b[0;0H\x1b[3J");
		//printf("\x1b[2J");
		render_single_box(a);
		printf("\nFrame: %d\n", frame);
		frame++;
		if (frame == 240) {
			boxit_close();
			break;
		}
		usleep(16667);
	}

	return 0;
}
