/*
 * This file is part of tss.
 * Copyright (C) 2022 Abdullah Çırağ
 *
 * tss is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU Affero General Public License as published by  
 * the Free Software Foundation, version 3 of the License only.  
 *
 * This program is distributed in the hope that it will be useful,  
 * but WITHOUT ANY WARRANTY; without even the implied warranty of  
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 * GNU Affero General Public License for more details.  
 *
 * You should have received a copy of the GNU Affero General Public License  
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.  
 */
#include "render.h"
#include "box.h"
#include <stdio.h>

int
render_single_box(Box* box)
{
	unsigned int i, j, n;
	n = 0;
	unsigned int fgn = box->foreground_color_count;
	unsigned int bgn = box->background_color_count;

	for (i = 0; i < box->min_height; i++) {
		for (j = 0; j < box->min_width; j++) {
			if (fgn > n) {
				printf("\x1b[38;2;%d;%d;%dm", (box->foreground_colors)[n][0],
				        (box->foreground_colors)[n][1],
				        (box->foreground_colors)[n][2]);
			}
			else {
				printf("\x1b[38;2;255;255;255m");
			}

			if (bgn > n) {
				printf("\x1b[48;2;%d;%d;%dm", (box->background_colors)[n][0],
				        (box->background_colors)[n][1],
				        (box->background_colors)[n][2]);
			}
			else {
				printf("\x1b[48;2;0;0;0m");
			}

			putchar(box->chars[n]);
			n++;
		}

		printf("\x1b[38;2;255;255;255m\x1b[48;2;0;0;0m│\n");
	}

	for (i = 0; i <= box->min_width; i++)
		printf("\x1b[38;2;255;255;255m\x1b[48;2;0;0;0m─");

	printf("\n");

	return 0;
}
