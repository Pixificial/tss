/*
 * This file is part of tss.
 * Copyright (C) 2022 Abdullah Çırağ
 *
 * tss is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU Affero General Public License as published by  
 * the Free Software Foundation, version 3 of the License only.  
 *
 * This program is distributed in the hope that it will be useful,  
 * but WITHOUT ANY WARRANTY; without even the implied warranty of  
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 * GNU Affero General Public License for more details.  
 *
 * You should have received a copy of the GNU Affero General Public License  
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.  
 */
#ifndef _BOX_H_
#define _BOX_H_
#define EMPTY_CHAR '.'

enum box_type {
	BOX_BOXB,
	BOX_BOXBC,
	BOX_BOXBCF,
	BOX_BOXBCFB,
	BOX_BOXBS
};

typedef struct box {
	unsigned int min_width;
	unsigned int min_height;
	enum box_type btype;
	char *chars;
	unsigned int char_count;
	unsigned int (*foreground_colors)[3];
	unsigned int foreground_color_count;
	unsigned int (*background_colors)[3];
	unsigned int background_color_count;
} Box;

Box	*box_create(unsigned int, unsigned int);
Box	*box_create_c(unsigned int, unsigned int, char);
Box	*box_create_cf(
        unsigned int,
		unsigned int,
		char,
		unsigned int (*)[3],
		unsigned int
		);
Box *box_create_cfb(
		unsigned int,
		unsigned int,
		char,
		unsigned int (*)[3],
		unsigned int,
		unsigned int (*)[3],
		unsigned int
		);
Box	*box_create_s(unsigned int, unsigned int, char *, unsigned int);

#endif
