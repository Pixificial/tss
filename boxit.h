/*
 * This file is part of tss.
 * Copyright (C) 2022 Abdullah Çırağ
 *
 * tss is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU Affero General Public License as published by  
 * the Free Software Foundation, version 3 of the License only.  
 *
 * This program is distributed in the hope that it will be useful,  
 * but WITHOUT ANY WARRANTY; without even the implied warranty of  
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 * GNU Affero General Public License for more details.  
 *
 * You should have received a copy of the GNU Affero General Public License  
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.  
 */
#ifndef _BOXIT_H_
#define _BOXIT_H_

#include "render.h"
#include <stddef.h>
#define MAX_BOX_COUNT 32
extern Box *boxes[MAX_BOX_COUNT];
extern size_t box_count;
/* int boxit_init(); */
int boxit_close();
#endif
