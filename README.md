# tss - A terminal side-scroller game.
## About
tss is a text based game for UNIX-like systems with a custom dynamic TUI.

## Compiling
To compile, you need:
1. C compiler of choice (assumed GCC).

#### *nix (GNU+Linux, OpenBSD etc.)
1. Clone the repository `git clone https://codeberg.org/Pixificial/tss.git`
2. `cd` into the local directory.
3. Run `gcc box.c render.c boxit.c main.c -o tss`

## License 
Copyright (C) 2022 Abdullah Çırağ

tss is free software: you can redistribute it and/or modify  
it under the terms of the GNU Affero General Public License as published by  
the Free Software Foundation, version 3 of the License only.  

This program is distributed in the hope that it will be useful,  
but WITHOUT ANY WARRANTY; without even the implied warranty of  
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
GNU Affero General Public License for more details.  

You should have received a copy of the GNU Affero General Public License  
along with this program.  If not, see <https://www.gnu.org/licenses/>.  
